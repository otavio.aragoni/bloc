# BLoC

## What is BLoC? 
- BLoC stands for Business Logic Component
- It is a design pattern
- Similar to MVC or MVVM
- Based on Streams
- The bloc package helps implement the design pattern

## Architecture
![](images/bloc_architecture_full.png)

- A bloc has a state that can be altered through an event that can be sent by the UI.
- The bloc will handle all the business logic and possible requests to the data layer depending on the event it received.
- The bloc will output the next state through the stream.

## Concepts
- Stream
- Cubit
- Bloc

## Packages

### [bloc](https://pub.dev/packages/bloc)
"A dart package that helps implement the BLoC pattern."

#### Blocs:
Class that will manage the state, receive events to change the state and output the next state. 

modified example from documentation:
```dart
/// The CounterState class
class CounterState {
  int counter;

  CounterState._();

  factory CounterState.initial() {
    return CounterState.()..counter = 0;
  }
}

/// The events which `CounterBloc` will react to.
abstract class CounterEvent {}

class IncrementEvent extends CounterEvent{}
class DecrementEvent extends CounterEvent{}

/// A `CounterBloc` which handles converting `CounterEvent`s into `int`s.
class CounterBloc extends Bloc<CounterEvent, CounterState> {
  /// The initial state of the `CounterBloc` is 0.
  CounterBloc() : super(CounterState.initial());

  @override
  Stream<CounterState> mapEventToState(CounterEvent event) async* {
    if (event is IncrementEvent) {
      yield state..counter += 1;
    } else if (event is DecrementEvent) {
      yield state..counter -= 1;
    }
  }
}
```

##### Sending events to Blocs
```dart
final counterBloc = CounterBloc();
counterBloc.add(IncrementEvent());
``` 

#### Cubits:
A Cubit is a simplified verison of a Bloc, actually they are both extended from BlocBase.
The main difference is that instead of events, the Cubit class expose public methods to emit new states.

```dart
class CounterCubit extends Cubit<int> {
  /// The initial state of the `CounterCubit` is 0.
  CounterCubit() : super(0);

  /// When increment is called, the current state
  /// of the cubit is accessed via `state` and
  /// a new `state` is emitted via `emit`.
  void increment() => emit(state + 1);
  void decrement() => emit(state - 1);
}
```

##### Calling Cubits methods
```dart 
final counterCubit = CounterCubit();
cubit.increment();
```

### [flutter_bloc](https://pub.dev/packages/flutter_bloc)
"Widgets that make it easy to integrate blocs and cubits into Flutter. Built to work with package:bloc."

#### BlocBuilder
Widget that build a widget based on the changes on the bloc state.
```dart
BlocBuilder<CounterBloc, CounterState>(
  builder: (BuildContext context, CounterState state) {
    return Text('$state.counter');
  }
)
```

How do the BlocBuilder access the bloc instance?
- You can pass the bloc as a named param or
- You can use a BlocProvider

#### BlocProvider
**BlocProvider** provides a single instance of a bloc to its children.

```dart
BlocProvider(
  create: (BuildContext context) => CounterBloc(),
  child: ChildA(),
);
```

Every children of this BlocProvider will have access to the this counter bloc instance using the BlocBuilder.
The Bloc is provided via `BlocPorivder.of<T>(contex)`.

#### Other Widgets
- MultiBlocProvider
- BlocListener
- BlocConsumer

### References
- (documentation)[https://bloclibrary.dev/]
- (Reso Coder)[https://www.youtube.com/watch?v=LeLrsnHeCZY&t=675s&ab_channel=ResoCoder]
